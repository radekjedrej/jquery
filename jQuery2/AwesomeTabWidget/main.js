

$(document).ready(function(){

  $('.tab-header .item').on('click', function(){
    var number = $(this).data("option");

    // remove all actives
    $('.tab-header .item').removeClass('active');
    // add active
    $(this).addClass('active');
    // remove all active containers
    $('.container-item').hide();
    // add active class to container
    $('div[data-item="'+ number +'"]').show();

  });
});