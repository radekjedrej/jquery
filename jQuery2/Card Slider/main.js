

$(document).ready(function(){

  var itemsLength = $('.cards-container .item').length;
  var current = 1;
  
    $('.total-slides').text(itemsLength);
    $('.current-slide').text(current);
  // Set totl amount 



  $('.card-slider .btn-previous').on('click', function(){

    if(current > 1){
    current = current -1;
    showSlide(current);
    } else {
      current = itemsLength;
      showSlide(current);
    }
    
  });

  $('.card-slider .btn-next').on('click', function(){
    
  if(current !== itemsLength) {
    current = current + 1;
    showSlide(current);
    } else {
    current = 1
    showSlide(current);
  }


  });

  function showSlide(current) {
    $('.cards-container .item').removeClass('active');

    $('div[data-id="'+ current +'"]').addClass('active');

    $('.current-slide').text(current);

  }

});