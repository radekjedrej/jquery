

$(document).ready(function(){

  //PREPEND APEND
    //PREPEND APEND

  // var parent = document.querySelector(".parent");
  // var newElement = document.createElement("div");

  // newElement.className = "newDiv";
  // newElement.innerText = "My new div";

  // parent.appendChild(newElement);

  // $('.parent').append('<div class="newDiv">My new div</div>');

  // $('<div class="newDiv">My new div</div>').appendTo('.parent');

  // $('.parent').prepend('<div class="newDiv">My new div</div>');


  // BEFORE AFTER 
   // BEFORE AFTER

  //  $('.element').before('<div>Before</div>');
  //  $('.element').after('<div>Before</div>');

  // $('input').after('<div>Missing message</div>');

  // REPLACING ELEMENTS
    // REPLACING ELEMENTS

  // $('.replaceMe').replaceWith('<div>Replaced</div>');

  // $('<div>Replaced</div>').replaceAll("div");
  // $('<div>Replaced</div>').replaceAll(".replaceMe,.replaceMeAgain");

  // REMOVING ELEMENTS
    // REMOVING ELEMENTS

  // $('.removeMe').remove();

  // $('ul li:eq(1)').remove();

  // $('ul').empty();

  // $('ul').html('');

  // $('ul').append('<li>item6</li>');
  // $('ul').prepend('<li>item0</li>');


  // ACCESS ELEMENTS DATA
    // ACCESS ELEMENTS DATA

  //  console.log($('a').text());
  // $('a').text('Something else');
  
  // console.log($('a').attr("href"));
  // ($('a').attr("href","www.amazon.com"));
  // console.log($('a').attr("href"));

  // console.log($('a').attr("style"));
  // ($('a').attr("style","background: blue"));
  // console.log($('a').attr("style"));

  // console.log($('input').val());

  // console.log($('input').prop("checked"));

    // CLASSES
      // CLASSES

  // var classes = $('div').attr('class');


  // var check = ($('.element').hasClass('anotherClass'));


  // if(check) {
  //   console.log('has it');
  // }else {
  //   console.log('Nope');
  // }

  // $('.element').addClass('bottom');
  // $('.element').removeClass('bottom');

    // $('.element').toggleClass("anotherClass bottom");

    // var element = $('.element2');
    
    // console.log(element.css("width"));
    // console.log(element.css("fontSize"));

    // console.log(element.width());
    // console.log(element.height());

    // element.css('background','red');
    // element.css('font-size','20px');

    // element.css("width","+=50px");

    // element.css({
    //   "background":"blue",
    //   "font-size":"10",
    //   "margin-top":"100px"
    // })


        // DATA ATTRIBIUTES
    // DATA ATTRIBIUTES


    console.log($('.element3').data("hobbies"));
    console.log($('.element3').data("name"));

    console.log($('.element3').data());

    var cars = ["ford","chevy","honda"];

    $('.element3').data("cars",cars);
    console.log($('.element3').data());

});