

$(document).ready(function(){

  //AJAX1
  // $('.template').load("/templates/template.html .innerDiv",function(){
  //   console.log('done loading');
  // });

  //AJAX2
  // var METHOD = "GET";
  // var url = "https://jsonplaceholder.typicode.com/users";

  // $.ajax(url,{
  //   method: METHOD,
  //   success: function(data){
  //     console.log(data);
  //   },
  //   error: function(){
  //     console.log('Something went wrong');
  //   }
  // })

    //AJAX3
  var METHOD = "POST";
  var url = "https://jsonplaceholder.typicode.com/users";
  var DB = JSON.stringify({name:"Radek",lastname:"Jedrzejewski"});

  $.ajax(url,{
    method: METHOD,
    data: DB,
    dataType:"json",
    contentType:"application/json",
    success: function(data){
      console.log(data);
    },
    error: function(){
      console.log('Something went wrong');
    }
  })

});