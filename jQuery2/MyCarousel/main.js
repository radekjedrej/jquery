
$(document).ready(function(){


  var count = 1;
  var itemsLength = $('.my-card').length;

  $('.total').html(itemsLength);


  $('.control .control-previous').on('click', function(){
    console.log('hello');


    if(count == 1){
      count = itemsLength;
      showSlide(count);
    } else {
      count = count - 1;
      showSlide(count);
    }
    
  });

  $('.control .control-next').on('click', function(){
    console.log('hello');

    if(count !== itemsLength){
      count = count + 1;
      showSlide(count);
    }else{
      count = 1;
      showSlide(count);
    }


  });

  function showSlide(count){
    $('.my-card').removeClass('active');
    $('div[data-id="' + count + '"').addClass('active');
    $('.current').html(count);
  }
});

