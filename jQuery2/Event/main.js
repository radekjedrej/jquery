

$(document).ready(function(){

    // JavaScript

  // var element = document.querySelector('.element');

  // element.addEventListener('click', function(){
  //     console.log('click!');
  // });


    // JQuery

  // $('.element').click(function(){
  //   console.log('click');
  // });

  // MOUSE Events
   // MOUSE Events

  // $('.element').on('click mouseleave mouseover',function(){
  //   console.log('click');
  // });

    // Mouse Events
   // Mouse Events

  //  $('.element1').mouseenter(function(){
  //     console.log('mouse enter!');
  //  });

  //  $('.element1').mouseleave(function(){
  //   console.log('mouse leave!');
  //  });

  //  $('.element1').hover(function(){
  //   console.log('mouse enter!');
  //  },function(){
  //   console.log('mouse leave!');
  //  });

  //   $('.element1').mouseup(function(){
  //   console.log('mouse up!');
  //  });

  //  $('.element1').mousedown(function(){
  //   console.log('mouse down!');
  //  });

  //  $('.element1').mousemove(function(){
  //   console.log('mouse move!');
  //  });

  // $('.element1').click(function(e){
  //   console.log(e);
  //  });

  // $('.element1').mousemove(function(e){

  //   var pageCords = "(" + e.pageX + "," + e.pageY + ")";

  //   console.log("Cords:" +pageCords);
  //  });


     // KEYBOARD Events
   // KEYBOARD Events

  //  $(document).keydown(function(e){
  //     console.log(e.which);

  //     var down = 40;
  //     var right = 39;
  //     var left = 37;
  //     var up = 38;


  //     if (e.which === down) {
  //         $('.element2').css({
  //           top:"+=10px"
  //         });
  //     }

  //     if (e.which === right) {
  //       $('.element2').css({
  //         left:"+=10px"
  //       });
  //     }

  //     if (e.which === left) {
  //       $('.element2').css({
  //         left:"-=10px"
  //       });
  //     }

  //     if (e.which === up) {
  //       $('.element2').css({
  //         top:"-=10px"
  //       });
  //     }

  //     $(document).keyup(function(e){
  //       console.log(e.which);
  
  //       var down = 40;
  //       var right = 39;
  //       var left = 37;
  //       var up = 38;
  
  
  //       if (e.which === down) {
  //           $('.element2').css({
  //             top:"+=10px"
  //           });
  //       }
  
  //       if (e.which === right) {
  //         $('.element2').css({
  //           left:"+=10px"
  //         });
  //       }
  
  //       if (e.which === left) {
  //         $('.element2').css({
  //           left:"-=10px"
  //         });
  //       }
  
  //       if (e.which === up) {
  //         $('.element2').css({
  //           top:"-=10px"
  //         });
  //       }

  //  });

     // Scroll Events
   // Scroll Events

  //  $(document).scroll(function(){
  //     console.log('scrolling');
  //  });

  //  $(window).resize(function(e){
  //    console.log('resize');
  //  });

  // $('.element3').click(function() {
  //   console.log('click');
  // })

  // $(document).on('click','.element3',function(){
  //   console.log('click');
  // })

  // $('input').focusin(function(){ 
  //   console.log("focus in");
  // });

  // $('input').focusout(function(){
  //   console.log("focus out");
  // });

  // $('input:eq(1)').blur(function(e){
    // Java script way of doing this
      // var element = e.target.value;
      // console.log(element);
    // Jquery way of doing this
  //     var value = $(this).val();
  //     console.log(value);

  //     if (value.length <= 4) {
  //         console.log('perfect');
  //         $(this).addClass('error');
  //     }else{
  //         console.log('wrong');
  //         $(this).removeClass('error');
  //     }
  // });

       // SUBMIT Events
   // SUBMIT Events

  //  $('form').submit(function(e){
  //    e.preventDefault();

  //    console.log('not loading');
  //  })

  $('input').change(function(){
    console.log('changed');
  });

  $('input[type="checkbox"]').change(function(){
    console.log('changed');

    var checked = $(this).is(":checked");

    if(checked){
      console.log($(this).val() + " is checked");
    } else{
      console.log($(this).val() + " not checked");
    }
  });


});