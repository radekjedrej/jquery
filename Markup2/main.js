/* global $ */
/* global document */


$(document).ready(function () {


	var itemsLength = $('.cards_container .item').length;
	var current = 1;
	
	//Set total amount
	$('.total_slides').text(itemsLength);


	$('.cardSlider .btn_prev').on('click', function () {

		if (current > 1) {
			current = current - 1;
			showSlide(current);
		} else {
			current = 3;
			showSlide(current);
		}

	});


	$('.cardSlider .btn_next').on('click', function () {

		if (current != itemsLength) {
			current = current + 1;
			showSlide(current);
		} else {
			current = 1;
			showSlide(current);
		}



	});

	function showSlide(current) {
		$('.cards_container .item').removeClass('active');

		$('div[data-id="' + current + '"]').addClass('active');
		
		//Set current item
		$('.current_slide').text(current);



	}


});
